---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "5th Allpix Squared User Workshop"
date: 2024-02-15T08:57:12+01:00
---

We are happy to announce the **5th Allpix Squared User Workshop**, which will be held **from the 22nd to 24th of May 2024** at the beautiful University of Oxford, UK. For a lively workshop and plenty of discussions, in-person participation is strongly encouraged, but a remote connection will also be set up for participants who are unable to join in person.

The aim of this workshop is to bring the users and developers together. We will be happy to hear about your simulations and to discuss interesting applications and new features. In addition there will be introductory talks, case studies, expert talks and presentations about the development of the framework.

We kindly ask you to **register** through the [Indico page of the event](https://indico.cern.ch/e/apsqws5), where you can also **submit an abstract** to present your use case of or developments on Allpix Squared. We encourage contributions from all the fields that benefit from this simulation framework. The abstract submission deadline is the 22nd of April and registration for in-person attendance will be possible until May the 4th. The workshop fee covers the workshop dinner and optional lunches and depends on the participation in these activities.

We look forward to contributions and participation from new and experienced users as well as developers!

The poster of the event can be downloaded [here](/pdf/apsq_workshop_5_poster.pdf).

{{< figure src="/img/apsq_workshop_5_poster.png">}}

