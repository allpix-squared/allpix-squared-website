---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Patch Release 2.2.1"
date: 2022-03-22T12:04:31+01:00
draft: false
---

We would like to announce the first patch release for the 2.2 series of **Allpix Squared, version 2.2.1**.
This release contains 23 commits over the feature release version 2.2.0.
The release is available as Docker image, CVMFS installation, binary tarball and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->

* **Core**
   * Accidentally (since basically ever) the position of `Pixel` objects in the local coordinate system was placed at the backside of the sensor instead at the center plane. This affects the local coordinate position of `PixelCharge` and `PixelHit` objects written to file. Thanks for reporting [in the forums](https://allpix-squared-forum.web.cern.ch/t/propagatedcharge-with-0-charge-using-transientpropagation/264)!
   * The `allpix` executable now also reports the platform and hardware concurrency of the system it is running on - this might help us catch some issues:

       ```
       Allpix Squared version v2.2.1
                      built on 2022-03-15, 20:06:34 UTC
                      using Boost.Random 1.77.0
                            ROOT 6.24/06
                            Geant4 10.7.2
                      running on 96x AMD EPYC 7402 24-Core Processor

       Copyright (c) 2016-2022 CERN and the Allpix Squared authors.
       ```

   * In some cases, a run was not stopped ot interrupted with `CTRL+C` or `CTRL+\`. This was the case when all events have already been submitted to the worker queue, but haven't finished yet. This is fixed now and interruption of the run should also work in those cases.
* **Module DepositionGeant4:**
   * There was a bug in the code that would associate `MCParticle` objects of secondaries that are produced outside the sensor volume with the primary `MCParticle` object if it first passed the sensor, then created the secondary, which subsequently backscattered into the sensor. According to our definition of `MCParticles`, the second particle should be treated as primary to the sensor because it originated outside the sensor volume. Now this is properly taken care of. Also here, thanks for reporting [in the forums](https://allpix-squared-forum.web.cern.ch/t/particle-incidence-direction/268/)!
   * New histograms are produced, displaying the deposited energy per event per sensor, before conversion to e/h pairs.
