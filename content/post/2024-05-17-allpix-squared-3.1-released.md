---
# SPDX-FileCopyrightText: 2024 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Allpix Squared 3.1 Released"
date: 2024-05-17T11:36:51+01:00
draft: false
---

We are happy to announce a new stable feature release **Allpix Squared, version 3.1** with a total of **424 commits** added
to the repository by **17 contributors** since version 3.0.0. The release tarball can be downloaded from the repository:

https://gitlab.cern.ch/allpix-squared/allpix-squared/tags/v3.1.0

The new version is also available as docker image in the [project's docker registry](https://gitlab.cern.ch/allpix-squared/allpix-squared/container_registry) and is provided via [the CVMFS packages](https://allpix-squared.docs.cern.ch/docs/11_devtools/04_deployment/#software-deployment-to-cvmfs).


<!--more-->

### Implementation of a Fast Canali Mobility Model

A significant time of the simulation is often spent calculating charge carrier mobilities. [MR!1012](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1012) adds a "new" mobility model called *CanaliFast* which provides a significantly faster calculation of charge carrier mobilities from the Canali model. The main culprit of the model's function are the two calls to `std::pow(x, y)`:

```math
\mu(E) = \frac{v_m}{E_c} \frac{1}{\left(1 + (E / E_c)^\beta \right)^{1 / \beta}}
```

where $`\beta`$ is a floating-point number. In some simulations, exactly these calculations were representing by far the hottest code snippet of the entire framework.

The CanaliFast model is an alternative implementation of the Canali model described above. Instead of calculating the powers
$`x^\beta`$ and $`y^{1 / \beta}`$ directly for every requested mobility value, it uses pre-calculated lookup tables with
fixed binning and interpolates between the nearest bins. Depending on the simulation settings, this can provide a speed-up
of more than 30%.

The boundary values and the binning are chosen according to the expected range of the base. For the Canali model, values from
zero up to a field strength of 1000kV/cm are tabulated in 1000 bins. Separate lookup tables are built for the two power
calculations for electrons and holes, respectively.

For the calculation of $`x^\beta`$, the lower boundary is set to 0 since the electric field strength is positive. The upper
boundary is set to the argument of the Canali model formula, i.e. the maximum field strength divided by the
critical field strength provided by the model.

For the calculation of $`y^{1 / \beta}`$, the lower boundary is set to 1 owing to the offset present in the Canali formula.
The upper boundary is again set to the formula argument, i.e. $`1 + (E / E_C)^\beta`$ with $`E`$ being the maximum tabulated
field strength.

For field strengths outside the range, the first and last bin are extrapolated linearly, respectively.

The following plots show a comparison of the mobility and velocity of electrons and holes as calculated from the Canali and
the CanaliFast models. The maximum relative difference occurs at very low electric field strengths and is less than 0.004.

{{< figure src="/img/canali_fast_mobility.png" caption="Comparison of the electron and hole mobilities calculated using the Canali and CanaliFast models as a function of the electric field strength." >}}

{{< figure src="/img/canali_fast_velocity.png" caption="Comparison of the electron and hole velocities calculated using the Canali and CanaliFast models as a function of the electric field strength." >}}



### DepositionPointCharge: Deposit scan in 1D and 2D

The DepositionPointCharge module has received a significant improvement to its scanning functionality in [MR!1084](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1084).
It is now possible to only scan the deposition point through the pixel cell in one or two dimensions instead of all three.
This can be very useful if one for example wants to check the arrival time from different positions in the x-y plane.

This feature introduces the new keyword `scan_coordinates`, which takes a combination of the values x, y and z. If only one or two values are defined, a 1D or 2D scan is performed.
The `position` parameter is used to set the positions for the directions that are not scanned.
Output plots have been added to show the deposition position in the three planes *xy*, *xz*, and *yz*. The keyword `output_plots_bins_per_um` is used to define how many bins per micrometer will be used in the output plots.
An example configuration that performs a scan in the x-z plane with a fixed y-position of 10um is shown below:

```ini
[DepositionPointCharge]
source_type = "point"
model = "scan"
scan_coordinates = x z
position = 0um 10um 0um
number_of_charges = 1000
output_plots = true
output_plots_bins_per_um = 5
```

{{< figure src="/img/scan_plane_arrival.png" caption="Examples for the simulation of charge carrier arrival times at the electrode which all emanate from the same x-y plane on the pixel cell." >}}

### DepositionGeant4: Improvements Beam Shapes

Beams simulated with the DepositionGeant4 module can now have a `focus_point` instead of a `beam_divergence` in order to simulate beams focused at a specific point in the setup, via [MR!1104](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1104).
Furthermore, [MR!1077](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1077) implemented the possibility to simulate a rectangular or elliptic beam.
The dimensions are specified with the `beam_size` parameter, by providing either one or two values.

* If only one value is specified, the dimensions in *x* and *y* are equal, i.e. the width of the rectangle or the major axes of the ellipse.
* If two values are specified, they should correspond to respective parameters in *x* and *y*.

Backwards capability is ensured when using the default circle beam with one `beam_size` value for the beam sigma in r.

A configuration for a rectangular beam reads as follows:

```ini
[DepositionGeant4]
source_type = "beam"
particle_type = "e-"
source_energy = 5GeV
beam_size = 3mm 4mm
beam_shape = Rectangle
beam_direction = 0 0 1
model = "fixed"
source_position =  0 0 -10mm
```


### Other Notable Features and Improvements

* **Core / CI**:
    * LCG has been updated to version 105 and Red Hat Enterprise Linux 9 (RHEL9/ALMA9) is now the default supported operating system.
    * Significant performance improvement in field lookup routine, only calculating transformations where necessary [MR!1011](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1011), [MR!1009](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1009)
    * Possibility to select dopant type (n or p) for some mobility models [MR!1016](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1016)
    * Support for newer macOS systems and corner cases of compilers installed via anaconda
    * Automatic building and testing of macOS ash been removed for now from the CI due to too many manual interventions on the mac runners.
    * Allpix Squared now also warns for unused keys in detector geometry and model files [MR!1075](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1075)
    * `MCParticle` objects now provide their initial and final kinetic energy [MR!1082](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1082)
* **Documentation**:
    * Various improvements, extensions of documentation chapters
    * I/O of modules are now printed in the summary table at the top of the module documentation [MR!1008](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1008)
    * Improved simulation geometry section.
    * More detailed description of Runge-Kutta-Fehlberg method, also providing the corresponding Butcher tableaus.
* **DefaultDigitizer**: This module from now on **does not** apply a default threshold, the `threshold` parameter needs to be explicitly set [MR!1018](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1018)
* **GeometryBuilderG4**:
    * Register Exception Handler to properly catch and process all Geant4 exceptions thrown during the run
    * Now supports cone shapes [MR!1110](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1110)
* **CorryvreckanWriter**: now transforms MCParticles correctly into the Cooryvreckan coordinate system [MR!996](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/996)
* **TransientPropagation**:
    * Now surface effects can be modeled using a relatively simple "reflection" model [MR!1041](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1041)
    * Refactoring of the propagation code for better performance and readability. Reduction of the Runge-Kutta algorithm from 5th-order RKF to 4th-order RK. The 5th order can be used for uncertainty estimation but is unused in this module because we run with fixed time steps. [MR!1043](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1043)
* **DepositionLaser**: Beam waist definition now adheres to commonly used definition [MR!1039](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1039)
* **DepositionGeant4**:
    * Fix issue with type conversion in calculating number of deposited charge carriers [MR!982](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/982)
    * Fixes to finding and retrieving Geant4 data directories with newer Geant4 version > 11.1.0
    * The additional "microelec" Physics Lists is now available [MR!1095](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1095)
    * Allpix Squared now registers a step hook to allow aborting events with unphysical particle steps [MR!1053](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1053)
* **WeightingPotentialReader**: Allow constraining the depth of an applied weighting potential using the `potential_depth` parameter [MR!1048](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1048)
* **CSADigitizer**: Improvements to documentation and parameter naming
* **DetectorHistogrammer**: Global detector hit maps now respect the global position of the detector and select the axis ranges properly [MR!1055](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1055)
* **Tools**:
    * New example for applying an eta correction [MR!1056](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/1056)
    * The mesh converter documentation now also mentions how to import fields from Silvaco Atlas.
