---
# SPDX-FileCopyrightText: 2020 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Patch Release 1.5.1"
date: 2020-07-24T10:22:13+02:00
draft: false
---

We are happy to announce the release of the first patch of the 1.5 series of **Allpix Squared, version 1.5.1**.
It contains 28 commits, mostly with minor improvements such as typos.
The release is available as Docker image, CVMFS installation, binary tarball and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->

* **Examples:** New example demonstrating the simulation of a [EUDET-type beam telescope](https://link.springer.com/article/10.1140/epjti/s40485-016-0033-2)
* **Manual:** Corrected some typos and journal references
* **Build system:** Detector models are now correctly installed again, git hooks have been corrected to properly detect tags, CMake regex escaping in test pass conditions has been improved.
* **CI:** Now completely moved to the latest LCG_97python3 release, added new job for spell checking in code
* **Module TransientPropagation:** Correctly reflect module status "Functional" in README
* **Module DepositionReader:** Some corrections to the module README
* **Tools / MeshConverter:** The TCAD MeshConverter tool now properly reports if a requested region is not found
