---
title: "Patch Release 2.3.3"
date: 2022-11-23T14:11:54+01:00
draft: false
---

We would like to announce the third patch release for the 2.3 series of **Allpix Squared, version 2.3.3**.
This release contains 22 commits over the feature release version 2.3.2.
The release is available as [Docker image](https://gitlab.cern.ch/allpix-squared/allpix-squared/container_registry/1137), CVMFS installation, [binary tarball](https://cern.ch/allpix-squared/releases) and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->


* **Core**: There have been several changes to the central ModuleManager which takes care of module loading and simulation execution. We have implemented improvements to the logic of locking resources in highly parallel situations and were able to remove some bottlenecks that appeared when running simulations with more than 30 worker threads in some setups. More details in the respective [MR!885](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/885).
* **Build System**:
    * The CMake check for the current version now also works in situations where the repository has been cloned with the `--notags` option as is the case for some CI jobs.
    * CTest now re-runs failed individual tests a second time to better catch random failures due to problematic CI job nodes.
* **MeshConverter**: The converter now validates all input, including the observable units, before starting the interpolation task. Before, an invalid unit would only be caught after the interpolation had finished, but as a result, the output was not stored.
