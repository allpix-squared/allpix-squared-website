---
# SPDX-FileCopyrightText: 2024 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Save the date: APSQWS6"
date: 2024-12-12T16:40:12+01:00
---

We are happy to announce that the **6th Allpix Squared User Workshop** will be held **from the 7th to 9th of May 2025**.
The workshop will take place at Nikhef in Amsterdam, and we encourage in-person participation. A remote connection will also be set up however, for participants who are unable to join in person.

More information will become available on the [Indico page of the event](https://indico.cern.ch/e/apsqws6) as well as on the website and in an announcement email early next year. We look forward to contributions from both users and developers!

If you have any questions about the workshop, feel free to contact us at allpix-squared-workshop@cern.ch
