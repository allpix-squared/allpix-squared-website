---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Publications"
menu:
  main:
    weight: 20
---

{{< blocks/lead height="auto" color="secondary">}}
<h2 style="padding-bottom: 1em;">
    Publications
</h2>
<span class="apsq-lead-text">
    Reference publications, presentation slide decks and tutorial resources on Allpix Squared.
</span>
{{< /blocks/lead >}}

<div class="container l-container--padded">

{{% blocks/section type="section" color="white" %}}

# Reference Publications


{{< citation title="**Allpix Squared: A modular simulation framework for silicon detectors**" authors="S. Spannagel et al." journal="Nucl. Instr. Meth. A 901 (2018) 164 – 172" reference="[doi:10.1016/j.nima.2018.06.020](https://doi.org/10.1016/j.nima.2018.06.020), [arXiv:1806.05813](https://arxiv.org/abs/1806.05813)" >}}


{{< citation authors="D. Dannheim et al." journal="Nucl. Instr. Meth. A 964 (2020)" title="**Combining TCAD and Monte Carlo methods to simulate CMOS pixel sensors with a small collection electrode using the Allpix Squared framework**" reference="[doi:10.1016/j.nima.2018.06.020](https://doi.org/10.1016/j.nima.2020.163784), [arXiv:2002.12602](https://arxiv.org/abs/2002.12602)" >}}

{{< citation authors="S. Spannagel and P. Schütze" title="**Allpix Squared - Silicon Detector Monte Carlo Simulations for Particle Physics and Beyond**" journal="J. Instr. 17 C09024 (2022)" reference="[doi:10.1088/1748-0221/17/09/C09024](https://doi.org/10.1088/1748-0221/17/09/C09024) [arXiv:2112.08642](https://arxiv.org/abs/2112.08642)" >}}

{{< citation authors="R. Ballabriga et al." title="**Transient Monte Carlo Simulations for the Optimisation and Characterisation of Monolithic Silicon Sensors**" journal="Nucl. Instr. Meth. A 1031 (2022) 166491" reference="[doi:10.1016/j.nima.2022.166491](https://doi.org/10.1016/j.nima.2022.166491), [arXiv:2202.03221](https://arxiv.org/abs/2202.03221)" >}}

Many publications employing Allpix Squared can be found [on InspireHEP](https://inspirehep.net/literature?sort=mostrecent&q=refersto%3Arecid%3A1678119) and on [Google Scholar](https://scholar.google.com/scholar?cites=16695938961228329897).
{{% /blocks/section %}}


{{% blocks/section type="section" color="white" %}}
# Workshops & Tutorials


{{< citation title="**4th Allpix Squared User Workshop**" reference="[https://indico.cern.ch/e/apsqws4](https://indico.cern.ch/e/apsqws4)" desc="*22-23 May 2023, DESY Hamburg, Germany*" >}}

{{% citation title="**3rd Allpix Squared User Workshop**" reference="[https://indico.cern.ch/e/apsqws3](https://indico.cern.ch/e/apsqws3)" desc="*09-11 May 2022, Virtual*" %}}

{{% citation title="**2nd Allpix Squared User Workshop**" reference="[https://indico.cern.ch/e/apsqws2](https://indico.cern.ch/e/apsqws2)" desc="*17-19 August 2021, Virtual*" %}}

{{% citation  authors="P. Schütze, S. Spannagel" title="**9th Beam Telescopes and Test Beams Workshop - Hands-On Tutorials**" reference="[Beginners](https://indico.cern.ch/event/945675/contributions/4181048/), [Advanced](https://indico.cern.ch/event/945675/contributions/4181049/)" desc="Hands-On: Silicon Detector Monte-Carlo Simulations with Allpix Squared - Beginners<br />Hands-On: Everything you would like to know about Allpix Squared - Advanced<br />*08-10 February 2021, Virtual*" %}}

{{% citation authors="D. Hynds, S. Spannagel" title="**8th Beam Telescopes and Test Beams Workshop - Hands-On Tutorial**" reference="[https://indico.cern.ch/event/813822/contributions/3648304/](https://indico.cern.ch/event/813822/contributions/3648304/)" desc="Silicon Detector Monte-Carlo Simulations with Allpix Squared<br />*27-31 January 2020, Tbilisi, Georgia*" %}}

{{% citation  authors="S. Spannagel" title="**7th Beam Telescopes and Test Beams Workshop - Hands-On Tutorial**" reference="[https://indico.cern.ch/event/813822/contributions/3648304/](https://indico.cern.ch/event/731649/contributions/3253672/)" desc="The Allpix Squared Simulation Framework<br />*14-18 January 2019, CERN, Switzerland*" %}}

{{% citation title="**1st Allpix Squared User Workshop**" reference="[https://indico.cern.ch/event/738283/](https://indico.cern.ch/event/738283/)" desc="*26-27 November 2018, CERN, Switzerland*" %}}

{{% citation  authors="S. Spannagel" title="**6th Beam Telescopes and Test Beams Workshop - Hands-On Tutorial**" reference="[https://indico.desy.de/indico/event/18050/session/3/contribution/65](https://indico.desy.de/indico/event/18050/session/3/contribution/65)" desc="The Allpix Squared Simulation Framework<br />*16-19 January 2018, Zurich, Switzerland*" %}}

{{% /blocks/section %}}


{{% blocks/section type="section" color="white" %}}
# Presentations & Seminars

S. Spannagel, [**Allpix Squared - Silicon Detector Monte Carlo Simulations for Particle Physics and Beyond**](https://dpnc.unige.ch/seminaire/talks/2021_22_allPix.pdf),
Université de Genève DPNC Seminar,
*27 October 2021, Geneva, Switzerland*

S. Spannagel, [**Allpix Squared - Silicon Detector Monte Carlo Simulations for Particle Physics and Beyond**](https://indico.cern.ch/event/797047/contributions/4455189/),
12th International Conference on Position Sensitive Detectors,
*17 September 2021, Birmingham, UK*

S. Spannagel, [**What's new on Allpix Squared?**](https://indico.cern.ch/event/945675/contributions/4159534/),
9th Beam Telescopes and Test Beams Workshop,
*08-10 February 2021, Virtual*

P. Schütze, [**Status and Plans for Allpix Squared**](https://indico.cern.ch/event/984049/),
CERN EP R&D WP1.4 Meeting: Monolithic Sensor Simulations,
*16 December 2020, Virtual*

S. Spannagel, [**The Allpix Squared Framework: New Developments**](https://indico.cern.ch/event/958135/contributions/4030964/),
CLIC Detector and Physics Collaboration,
*01 October 2020, Virtual*

S. Spannagel, [**The Importance of Insight: Monte Carlo Simulations in Silicon Pixel Detector Development**](https://instrumentationseminar.desy.de/sites2009/site_instrumentationseminar/content/e70397/e282395/e289794/2019-08-09-DESY-Instrumentation-Seminar_MC-Si-Detectors.pdf),
DESY Joint Instrumentation Seminar,
*09 August 2019, DESY, Hamburg, Germany*

S. Spannagel, [**Monte Carlo Simulations for Silicon Detectors: Bridging the Gap between Detector Design and Prototype Testing**](https://indico.cern.ch/event/811852/),
CERN Detector Seminar,
*12 April 2019, CERN, Switzerland*

S. Spannagel, [**The Allpix Squared Framework**](https://indico.cern.ch/event/804477/),
EP-SFT Group Meeting,
*8 April 2019, CERN, Switzerland*

S. Spannagel, [**Status and Plans for Allpix Squared**](https://indico.cern.ch/event/753671/contributions/3278131/),
CLIC Workshop,
*21-25 January 2019, CERN, Switzerland*

S. Spannagel, [**Updates on Allpix Squared**](https://indico.cern.ch/event/731649/contributions/3237196/),
7th Beam Telescopes and Test Beams Workshop,
*14-18 January 2019, CERN, Switzerland*

T. Billoud, **New features of the Allpix Squared simulation framework**,
Medipix Collaboration Meeting,
*19 September 2018, CERN, Switzerland*

S. Spannagel, [**Combining TCAD and Monte Carlo Methods to Simulate High-Resistivity CMOS Pixel Detectors using the Allpix Squared Framework**](https://indico.cern.ch/event/703821/contributions/3107881/),
CLIC Detector and Physics Collaboration Meeting,
*28-29 August, CERN, Switzerland*

P. Schütze, [**Allpix Squared - Recent Developments**](https://indico.cern.ch/event/703821/contributions/3107861/),
CLIC Detector and Physics Collaboration Meeting,
*28-29 August, CERN, Switzerland*

V. Sonesten, [**Event-based Multi-Threading in Allpix Squared**](https://indico.cern.ch/event/748000/),
*9 August 2018, CERN, Switzerland*

S. Spannagel, [**Allpix Squared - A Generic Pixel Detector Simulation Framework**](https://indico.cern.ch/event/656356/contributions/2848670/),
CLIC Workshop,
*22-26 January 2018, CERN, Switzerland*

S. Spannagel, [**Allpix Squared - A Generic Pixel Detector Simulation Framework**](https://indico.desy.de/indico/event/18050/session/10/contribution/0),
6th Beam Telescopes and Test Beams Workshop,
*16-19 January 2018, Zurich, Switzerland*

D. Hynds, **Allpix Squared - a Generic Pixel Detector Simulation Framework**,
Medipix Collaboration Meeting,
*29 November 2017, CERN, Switzerland*

S. Spannagel, [**Allpix Squared - A Generic Pixel Detector Simulation Framework**](https://indico.cern.ch/event/663851/contributions/2788161/),
31st RD50 Workshop,
*20-22 November 2017, CERN, Switzerland*

K. Wolters, [**The Allpix2 Simulation Framework**](https://indico.cern.ch/event/633975/contributions/2686446/),
CLIC Detector and Physics Collaboration Meeting,
*29-30 August 2017, CERN, Switzerland*

{{% /blocks/section %}}


</div>
