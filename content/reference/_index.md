---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Allpix Squared API Reference"
linkTitle: "API"
menu:
  main:
    weight: 60
    pre: <i class='fa-solid fa-code'></i>

cascade:
  - type: "docs"

exclude_search: true
---

**This is the Doxygen API reference of the Allpix Squared framework.**
This is neither an introduction nor the user manual of the framework, for this please refer to the [User manual](../docs/).

This documentation describes all classes and functions that are part of the framework.
It is automatically generated from the source code and is kept up to date to include the latest changes to the software.

The reference can be used and accessed in three different ways.
First, the *modules* section combines related functionality under a common category.
The *classes* section contains a list of all classes and the subclasses.
Finally the *files* section provides a list of all the files in the framework and everything defined in those files.

